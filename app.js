const cheerio = require('cheerio');
const request = require('request');
const io = require('socket.io-client');
const HTTPS = require("https");

let defaultAgent = new HTTPS.Agent();
let url = 'https://www.hltv.org/matches/2341755/natus-vincere-vs-mousesports-gamers-without-borders-2020';

request({
    url: url,
    headers: {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'
    }
  },
  (error, response, html) => {
    if (!error && response.statusCode === 200) {
      const $ = cheerio.load(html);

      request
        .get({
          method: 'GET',
          uri: 'https://cf3-scorebot.hltv.org/socket.io/?EIO=3&transport=polling&t=N9i7aml',
          gzip: true
        })
        .on('response', function (response) {
          let myString = (response.headers['set-cookie']);
          let sidBefore = myString[1];
          let sId = sidBefore.slice(3);
          console.log(sId+'ulala');

          let url = $('#scoreboardElement')
            .attr('data-scorebot-url')
            .split(',')
            .pop();
          let matchId = $('#scoreboardElement').attr('data-scorebot-id');

          let socket = io.connect(url, {
            agent: defaultAgent,
            path: '/socket.io/?EIO=3&transport=polling&t=N9i7aml&sid' + sId,
          });

          socket.on('connect', () => {
            console.log(socket.id); // sid в этом конекшне отличается от запрошенного в GET (строка 19). разные соедининия
          });

          let initObject = JSON.stringify({
            token: 'N9i7aml',
            listId: matchId
          });

          socket.on('connect', function () {
            let done = () => {
              return socket.close();
            };

            socket.on('score', (data) => {
              console.log(data + ' this connected');
            });

            socket.emit('readyForScores', initObject);

            // error handling
            socket.on('connect_error', (error) => {
              console.log(error)
            });

            socket.on('connect_timeout', (timeout) => {
              console.log(timeout)
            });

            socket.on('error', (error) => {
              console.log(error)
            });

            done();
          });
        })
    }
  });